const marked = require('marked');
const glob = require('glob');
const fs = require('fs');
const path = require('path');

function template(parsedHtml) {
  return `<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/solarized-dark.min.css">
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <main>
${parsedHtml}
  </main>
  <footer>
    <p>2019-10-19 TachTalk</p>
  </footer>
</body>
</html>
`;
}

// Set options
// `highlight` example uses `highlight.js`
marked.setOptions({
  renderer: new marked.Renderer(),
  highlight: function(code) {
    return require('highlight.js').highlightAuto(code).value;
  },
  pedantic: false,
  gfm: true,
  breaks: false,
  sanitize: false,
  smartLists: true,
  smartypants: false,
  xhtml: false
});

glob('**/*.md', { ignore: 'node_modules/**/*' }, (err, files) => {
  if (err) {
    console.error(err);
    return;
  }

  files.forEach((value) => {
    const info = path.parse(value);
    let dir = info.dir;
    if (info.dir) {
      dir = `${dir}${path.sep}`;
    }
    const fileName = `${dir}${info.name}.html`;
    const source = fs.readFileSync(value, { encoding: 'utf8' });
    const html = marked(source);

    fs.writeFileSync(fileName, template(html), { encoding: 'utf8' });
  });
});
